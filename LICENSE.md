# Tulpen's Silly License
Version 1, 15th September 2023

Copyright (C) 2023 Tulpenkiste <https://tulpenkiste.codeberg.page>

Everyone is permitted to copy and distribute verbatim copies of this license document, albiet you are not permitted to edit the document.

## 0. Additional Definitions
As used herein, "this license" refers to version 1 of Tulpen's Silly License.

"Libraries" refers to any work that provides an interface that can be combined or linked against.
"Applications" refers to any work that combines source code into an executable format.
"Original Work" is work created by either one or multiple authors, not any modified versions by other individuals.
"Modified Work" is an modified version of the original work not creation by the original author(s).

"Author(s)" refers to the creator(s) of the original work using this license.

## 1. Modified Versions
If you modify a copy of a work under this license and distribute it in any way, shape or form, then you must provide a free copy of the source code of the modified work using either:
- a) The same license as the original work while still ensuring the part of its purpose is still operational.
- b) Under any copyleft licenses that the author(s) has explicitly provided written approval of.

## 2. Commercial Use
Under no circumstances may the user or redistributers of the original work profit off of the original work utilising this license without the written approval of the author(s).
This includes (but is not limited to):
- Reselling by corporations/for-profit organisations
- Private facilities
- Web-based services

## 3. Usage in military or police agencies
Under no circumstances may applications or libraries under this license be used by the military or law enforcement agencies.

## 4. Revised versions of Tulpen's Silly License
Tulpen, the author of this license, may publish a new revision of this license at any given point in time. Each revision is given an exclusive version number to differentiate itself between each version.

If a library or application you receive states a specific version number "or any later version", you must abide by the conditions laid out in either the version specified or the conditions laid out in any future revision of your choosing.
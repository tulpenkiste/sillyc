#!/bin/env bash

echo "SillyC v0.0.1-$(git rev-parse --short HEAD)"

TEST=0

function run_test {
	echo "Test $TEST - $1"
	gcc -I./include $1 -o tmp/test.o
	EXPECTED=$(cat "$1" | grep "EXPECTED_OUTPUT:" | sed 's/[^0-9]*//g')

	if [[ "$UNSUPPRESS_OUT" == "1" || "$UNSUPPRESS_OUT" == "true" ]]; then
		tmp/test.o
	else
		tmp/test.o > /dev/null
	fi
	
	RESULT=$?
	rm tmp/test.o

	if [[ "$EXPECTED" == "$RESULT" ]]; then
		echo "PASS: $1 WITH RESULT $EXPECTED"
		return 0
	else
		echo "FAIL: $1, EXPECTED $EXPECTED, GOT $RESULT"
		return 1
	fi
}

if [[ ! -v UNSUPPRESS_OUT ]]; then
	export UNSUPPRESS_OUT=0
fi

for file in ./tests/*.c; do
	run_test "$file"
	TEST=$((TEST+1))
done
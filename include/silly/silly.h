#ifndef SILLY_H
#define SILLY_H

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

#ifndef MAX_GRR
#define MAX_GRR 255
#endif

// Variables
uint64_t _grr_count = 0;

// Macros

// Symbols
#define mrrp =
#define prr ;
#define grr ; assert(!(_grr_count++ >= MAX_GRR));

// Constants
#define twue true
#define fawse false

// Flow
#define mreow return
#define ewseif } else if
#define then {
#define endif }
#define fur for

// IDk
#define unmeowed unsigned

// Types
#define abywss void
#define numbah int
#define longowo long
#define chawactar char
#define stwing char*
#define boowean bool
#define memowyaddwess *
#define awwayof(type) type*
#define addwessof(type) type*

// Functions
#define myain main
#define mawwoc malloc
#define demawwoc demalloc
#define pwintf printf
#define pawts puts

#endif

CC?=gcc

INCLUDE_DIRS=-I./include
OUTPUT_DIRECTORY=build

.PHONY: example clean always

example: always
	$(CC) $(INCLUDE_DIRS) example/silly.c -o $(OUTPUT_DIRECTORY)/silly.o

install: always
	sudo cp -r include/silly /usr/include

clean: always
	rm -r $(OUTPUT_DIRECTORY)

always:
	mkdir -p $(OUTPUT_DIRECTORY)